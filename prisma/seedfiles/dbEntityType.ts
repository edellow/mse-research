export const dbEntityTypeSeed = [
  { EntityType: "Sproc", TypeDesc: "Sprocs", TypeSortOrder: 3 },
  { EntityType: "Table", TypeDesc: "Tables", TypeSortOrder: 1 },
  {
    EntityType: "Ifunc",
    TypeDesc: "Inline Functions",
    TypeSortOrder: 5,
  },
  { EntityType: "Func", TypeDesc: "Scalar Functions", TypeSortOrder: 4 },
  { EntityType: "View", TypeDesc: "Views", TypeSortOrder: 2 },
];
