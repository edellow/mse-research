export const dbJobSeed = [
  {
    id: 2,
    Name: "ACCU Reports SSIS",
    Description:
      "Gets all the ACCU Reports via FTP from ACCU and sends all outbound ACCU Reports via FTP to ACCU. Done 8-5 hourly M-F weekdays..",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 18,
    Name: "ACCU Special ACH for LQ",
    Description:
      "Finance manual processing of provider payments and moving of other monies.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 56,
    Name: "ACCU SSIS for LQ",
    Description: "This job should only be run on week days.",
    StartStep: 9,
    Enabled: 1,
  },
  {
    id: 79,
    Name: "Archive VSE Log Data",
    Description:
      "Runs weekly to move log records over one year old in the VSE database from the LOG table to the LOGarchive table.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 62,
    Name: "ASG",
    Description:
      "11/20/21, KAW:  FTP script file on server was modified to not send data files to ASG.  That is now a function of MemberAdmin.  The script still pulls data down BadAddress and Unfulfilled files from the vendor server for manual processing.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 36,
    Name: "Careington",
    Description:
      "Generate and FTP Careington a full or delta eligilibility file. Full on 6 days prior to end of month, delta on all other days of month.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 26,
    Name: "ClaimStatusAndEligibilityDataToChangeHealth",
    Description:
      "Files are generated in E:VSEFTPCSaEFilesToCH and then uploaded to the ChangeHealth SFTP shared site in WS_FTP in the clearinghouseinboundhdsinctcrm31 folder on a daily basis.  Files are moved to the E:VSEFTPCSaEArchive folder after they have been successfully transferred to the vendor server.  Credentials are available in KeePass under the ChangeHealth SFTP vendor entry.",
    StartStep: 1,
    Enabled: 0,
  },
  {
    id: 80,
    Name: "ClaimStatusAndEligibilityErrorLogRetrieval",
    Description:
      "Error load reports are downloaded daily from the clearinghouseoutboundhdsoutctcrm31 folder on the ChangeHealth SFTP shared site.  These files are saved to E:VSEFTPCSaEReportFilesFromCH on the local server.  Credentials are available in KeePass under the ChangeHealth SFTP vendor entry.",
    StartStep: 1,
    Enabled: 0,
  },
  {
    id: 21,
    Name: "CSIMart DailyImport",
    Description:
      'This job imports the CSI Extract zip file, checks that files exist in the zipfile, replaces " | " with space in the data files and imports the records into the corresponding CSI (call and callhistoryitem) tables.',
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 31,
    Name: "DBA Maintenance - Cleanup ProcedureLog Table",
    Description:
      "Runs daily to keep the UtilityDB.dbo.ProcedureLog table from growing too large and affecting performance of stored procedures that write to the log table.  Retention interval can be set in job step by setting variable value.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 3,
    Name: "DBA Maintenance - CommandLog Cleanup",
    Description: "Source: https://ola.hallengren.com",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 27,
    Name: "DBA Maintenance - DatabaseBackup - SYSTEM_DATABASES - FULL",
    Description: "Source: https://ola.hallengren.com",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 19,
    Name: "DBA Maintenance - DatabaseBackup - USER_DATABASES - DIFF",
    Description: "Source: https://ola.hallengren.com",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 58,
    Name: "DBA Maintenance - DatabaseBackup - USER_DATABASES - FULL",
    Description: "Source: https://ola.hallengren.com",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 78,
    Name: "DBA Maintenance - DatabaseBackup - USER_DATABASES - LOG",
    Description: "Source: https://ola.hallengren.com",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 10,
    Name: "DBA Maintenance - Integrity Check - All Databases",
    Description: "Run DBCC CHECKDB for all local databases",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 24,
    Name: "DBA Maintenance - Monthly Billing - Add billing specific indexes and stats",
    Description:
      "Temporarily add indexes found to be beneficial in resolving billing bottlenecks",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 23,
    Name: "DBA Maintenance - Monthly Billing - Disable Indexes",
    Description:
      "Disable non-clustered indexes in Banking and CCMBilling databases with exceptions in CCMBilling",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 76,
    Name: "DBA Maintenance - Monthly Billing - Drop billing indexes and stats",
    Description:
      "Drop indexes and stats created for the sole purpose of boosting performance for monthly billing",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 34,
    Name: "DBA Maintenance - Monthly Billing - Full DB Backup",
    Description: "Used to create full backup of billing databases",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 12,
    Name: "DBA Maintenance - Monthly Billing - Post-Billing",
    Description: "No description available.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 45,
    Name: "DBA Maintenance - Monthly Billing - Pre-Billing",
    Description: "No description available.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 30,
    Name: "DBA Maintenance - Monthly Billing - Rebuild indexes",
    Description: "Rebuild indexes on Banking and CCMBilling databases",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 71,
    Name: "DBA Maintenance - Monthly Billing - Recover Databases",
    Description:
      "Run to recover DB backups up to a point in time.  Run each step in order to the point in time desired, then run step 7 to bring DBs online.<br>	Step 1 - Check if current date is a scheduled billing date<br>	Step 2 - Recover to pre-billing state<br>	Step 3 - Recover to just before invoice processing<br>	Step 4 - Recover to just before waives run<br>	Step 5 - Recover to just before copies of invoices archived<br>	Step 6 - Recover to point of billing completion<br>	Step 7 - Bring DBs back online.",
    StartStep: 2,
    Enabled: 1,
  },
  {
    id: 64,
    Name: "DBA Maintenance - Optimization - Indexes and Stats",
    Description:
      "Optimizes indexes and statistics and runs transaction log backup job (source for both maintenance routines is https://ola.hallengren.com).<br><br>Updates PE.dbo.MartImportStatus table with date/time stamp for post-nightly task.<br><br>Runs the MDM Mart Merge job.<br><br>Waits for transaction log backup job to complete and then runs PE Admin Job.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 46,
    Name: "DBA Maintenance - Output File Cleanup",
    Description: "Source: https://ola.hallengren.com",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 72,
    Name: "DBA Maintenance - Post-Nightly Job Cleanup",
    Description:
      "Performs check if ACCU SSIS for LQ job is still running (on weekdays), and if so, retries every five minutes for two hours.  Rebuilds indexes disabled before nightly job and then starts main DBA job.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 73,
    Name: "DBA Maintenance - Prepare Databases for Bulk-logged Transactions",
    Description:
      "Backs up transaction logs and disables indexes in key databases to boost nightly data load performance.  In the event of job failure steps may need to be performed manually to ensure nightly processing runs smoothly.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 37,
    Name: "DBA Maintenance - Set Database Options After Refresh",
    Description:
      "Run this job manually after restoring databases from PROD to enable query store and legacy cardinal estimation",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 16,
    Name: "DBA Maintenance - sp_delete_backuphistory",
    Description: "Source: https://ola.hallengren.com",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 74,
    Name: "DBA Maintenance - sp_purge_jobhistory",
    Description: "Source: https://ola.hallengren.com",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 5,
    Name: "DBA Maintenance - Statistics and Index",
    Description: "Perform daily index and statistics maintenance",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 14,
    Name: "DBA Maintenance - USER_DATABASES - DETERMINISTIC",
    Description: "Used to call FULL or DIFF backup jobs based on day of week",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 29,
    Name: "EFT Duplicate Entries",
    Description:
      "10.18.2017 - Notify Art Iacobi or Legacy Apps Team on failure - Description: Connects to the flme-dv-sql006 Server, Banking Database, and runs the View_EFTEffectiveDupDetail view.  This view displays the duplicate EFTs.  Sends emails to Dave McMillen and Art Iacobi with a table depicting the results of the view if there are no null values.  Sends emails to Beth Ingram, Stephanie Flinn, Arielle Horner, Dave McMillen and Art Iacobi with a table depicting the results of the view if there are null values.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 20,
    Name: "Eldo FTP",
    Description:
      "* Gets download file list from EnterpriseApps.dbo.EldoFileDownloads<br>* Checks to see if each file has been marked processed and skips if so<br>* Downloads file if needed and marks record in table as processed with timestamp<br>* Runs batch file to unzip all files and perform any other required file operations<br><br>Job waits until midnight to start the VSEDaily_EldoMart job and then resets processed flag for all files in the table to prepare for next run.<br><br>This job is designed to restart on failure.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 51,
    Name: "EldoICD9",
    Description:
      "Populates the EldDataPool database with ICD9 data from files downloaded from Eldorado.  The job then populates the EldoDataMart database from the data that has been loaded into the EldDataPool database.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 38,
    Name: "Emdeon EOS PDF",
    Description:
      "Downloads the explanation of shares (EOS) as PDFs from Emdeon, via FTP, and puts them in the appropriate date named folder in the “E:EmdeonEosPDF” folder on CCMFS002.  The PDFs are used on the member portal for members and staff to view.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 67,
    Name: "Emdeon EOS Post Cleanup",
    Description:
      "Clean out old Need Objects where we now have a PDF of the Need",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 63,
    Name: "FirstDrVisitEmail",
    Description: "Load up into [tbFirstDrVisitHistory] today's new entries.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 61,
    Name: "MailUnlimited Generate and FTP",
    Description: "Builds and FTPs the MUI_Data_Extract files to MUI.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 75,
    Name: "MailUnlimited PDF Pickup FTP and Split",
    Description:
      "Pulls all of the 4 PDF MailUnlimited files down, via FTP and writes the pdfs into separate files, then splits and moves each of the PDF files to: \\192.168.212.46Invoices.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 57,
    Name: "MDLive",
    Description:
      "Check current day of month and if it is between the 10th and 15th, pickup MDLive PDF, txt and Excel files and deliver/process them.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 17,
    Name: "MDM Mart Merge",
    Description:
      "Executes stored procedures to merge daily data from SalesforceMart and EldoDataMart into the EnterpriseApps database.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 53,
    Name: "MDM Provider Daily",
    Description:
      "Runs the Government Provider Import SSIS Package to download the latest provider list from US government and import to the EnterpriseApps database.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 48,
    Name: "MediShare Monthly Billing - Create Invoices",
    Description:
      "Job in which spCreateMonthlyInvoices logical processing units have been broken out into separate sprocs and called in separate steps.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 41,
    Name: "MediShare Monthly Billing - Part 1",
    Description:
      "Run the first half of the billing process. Up to the point where excel data cut/paste is done prior to waives.<br><br>If job fails at step 4 (Check for Duplicate Products), delete duplicate product and restart the job at step 7.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 59,
    Name: "MediShare Monthly Billing - Part 2",
    Description:
      "Run the second part of the billing process. After where Excel data cut/paste is done prior to waives.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 66,
    Name: "Multiplan Generate and FTP",
    Description:
      "Generates a file and sends it, via FTP, to Multiplan.  It tells Multiplan of the previous month’s discounts.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 40,
    Name: "navitus",
    Description: "No description available.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 60,
    Name: "Navitus Delete Zip File From FTP",
    Description: "No description available.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 44,
    Name: "Navitus File Process",
    Description: "No description available.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 4,
    Name: "NetSuiteMart",
    Description: "Downloads the NetSuite cloud database using ODBC.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 69,
    Name: "NetSuiteMart Full Tables - Quarterly",
    Description:
      'Used on quarterly basis to give full detail for Calls, Tasks, Opportunities in their respective "Full" tables in the NS mart. As of 2/8/2016 the Calls, Opportunities, and Tasks tables only contain 12 to 24 months of data. If columns get added to any of these tables then extra care needs to be used to add to the ODBC queries for all three download tables AND to the table/full table too.',
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 50,
    Name: "Output File Cleanup",
    Description: "Source: https://ola.hallengren.com",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 13,
    Name: "Pay By Check Duplicate Entries",
    Description:
      "10.18.2017 - Notify Art Iacobi or Legacy Apps Team on failure - Description: Connects to the flme-dv-sql006 Server, Banking Database, and runs the View_CheckPayDupDetail view.  This view displays the duplicate check pays.  Sends emails to DL Duplicate Pay by Check Entries that include Dedi Lyell, Doug Mann, Grace Meshell, Vanessa Sheridan, Dave McMillen and Art Iacobi with a table depicting the results of the view.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 77,
    Name: "PE - Admin",
    Description: "Calls individual PE jobs so they all run in parallel",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 70,
    Name: "PE - Build AHP Table",
    Description: "Truncates and reloads the PE.dbo.tbAHP table",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 42,
    Name: "PE - Build Duplicate Claims List Table",
    Description:
      "Truncates and reloads data into the PE.dbo.[Duplicate Claims List] table",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 6,
    Name: "PE - Build LarrysSpreadsheet Table",
    Description:
      "Truncates and reloads data into the CCMBilling.dbo.tmpLarrysSpreadSheet table",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 25,
    Name: "PE - Build LastPaidThruDate Table",
    Description:
      "Truncates and reloads data into the CCMBilling.dbo.LastPaidThruDate table",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 32,
    Name: "PE - Build Relationship Table",
    Description:
      "Truncates and reloads data into the PE.dbo.tbRelationshipDate table",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 33,
    Name: "Provider Data Import",
    Description:
      "Do a pickup and import of Provider Data through the day from ECI Eldorado system.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 8,
    Name: "Purge_APIRquestEvent",
    Description:
      "To purge all records older than 365 days old based on [EnterpriseApps].[dbo].[APIRequestEvents] column value.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 28,
    Name: "Record NetSuite File Sizes",
    Description:
      "03/01/2018, KAW:<br><br>Created job to track NetSuite download file sizes, so we can compare data sets on nights that run longer than normal.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 52,
    Name: "Reset Environment for Billing Testing",
    Description: "Restores DBs and performs some prep work for billing testing",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 1,
    Name: "SalesForce Refresh",
    Description:
      'Refreshes entire Salesforce cloud database to the SalesforceMart database for reporting purposes<br><br>Check \\CCMSQLL$DBAmpRefreshLogsSF_RefreshAll.log for errors.  Search the file for the term "failed" to locate errors.',
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 65,
    Name: "SalesForce Replicate",
    Description:
      "Replicate all SF objects to SalesforceMart and create primary and foreign keys.<br><br>Run this job after DBAmp upgrades.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 11,
    Name: "SSIS Server Maintenance Job",
    Description:
      "Runs every day. The job removes operation records from the database that are outside the retention window and maintains a maximum number of versions per project.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 54,
    Name: "syspolicy_purge_history",
    Description: "No description available.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 47,
    Name: "UpdateEDIProviderDiscounts",
    Description: "Updates EDI.dbo.ProviderDisc from CCMSQL",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 39,
    Name: "VSE CheckRunFileExport",
    Description: "Used on an as needed basis to recover from a problem.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 15,
    Name: "VSE CheckRunFileImport",
    Description:
      "Imports the check information from the file(s) produced every weekday by the CCM folks in Rock Falls, IL into the CCM production databases.  The names of the files are in the format “MMddyynn.dat”, where “MM” is the current month (i.e., “01” for January, ”12” for December), “dd” is the current date (i.e., ”01” through ”31”), “yy” is the last two digits of the current year (i.e., ”15” for year 2015, ”16” for year 2016, etc.), and “nn” is the 2-digit file number for a given date (i.e., ”01” is the first file",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 35,
    Name: "VSE EFT Generate and FTP for LQ",
    Description: "Builds the EFT files and sends them to LQ.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 9,
    Name: "VSE EOS Email",
    Description:
      "This job is run at 11 PM (after the 2:30 PM VSE SSIS job generates new EOS) so that NetSuite system isnt burdened during the day. It also is run around 4-5 AM after the VSEDaily_Eldomart job step for Check Run File Import.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 68,
    Name: "VSE Post VSE Daily",
    Description:
      "Copies unprocessed members, deposits and shares from the VSE_Staging database to the VSE database.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 55,
    Name: "VSE SSIS for LQ",
    Description: "Automated processing of provider payments.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 43,
    Name: "VSE SSIS for LQ Copy",
    Description: "Automated processing of provider payments.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 49,
    Name: "VSEDaily_EldoMart",
    Description:
      "Main nightly job which starts many other jobs.  Troubleshoot failures at step level.",
    StartStep: 1,
    Enabled: 1,
  },
  {
    id: 22,
    Name: "ZZZ - DBA Maintenance - Optimization - Indexes and Stats",
    Description: "Perform daily index and statistics maintenance",
    StartStep: 1,
    Enabled: 0,
  },
  {
    id: 7,
    Name: "ZZZ - Decommissioned - Eldo EDI 834",
    Description:
      "Gets new member information and member switches (i.e., from the $1250 AHP level to the $7000 AHP level) from Salesforce and NetSuiteMart and creates two files, one for new members and one for member switches, for Eldorado to import into their system.  A new member file is created only if new members exist in Salesforce/NetSuiteMart.  Likewise, a member switches file is created only if there are member switches in Salesforce/NetSuiteMart.",
    StartStep: 3,
    Enabled: 0,
  },
];
