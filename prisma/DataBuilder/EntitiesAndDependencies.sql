/*=============================================================================================================
EntitiesAndDependencies.sql
The SQL statements in this script are used to generate JSON data for the seed files.  It can be ran against 
production MSE or a reasonable facimile (e.g. flme-dv-sql009)
The results can then be pasted into the seedfiles
***************************************************************************************************************/


/**************************************************** ALL OBJECTS *************************************************/
DECLARE @AllEntities table (id int,
						  EntityId int, 
						  ChildCount int,
						  ChildLevels int,
                          DBName nvarchar(30),
						  SchemaName nvarchar(20),
						  EntityBaseName nvarchar(90),
						  EntityName nvarchar(200),
						  TypeSortOrder int,
						  EntityType nvarchar(10)
						  );

INSERT INTO @AllEntities (EntityId,id,ChildCount,ChildLevels,DBName,SchemaName,EntityBaseName,EntityName,TypeSortOrder,EntityType)
	EXEC sp_msforeachdb 'select 
 							o.object_id as EntityId,
							1 as id,
							0 as ChildCount,
							0 as ChildLevels,
							''?'' as DBName,
							s.name as SchemaName,
							o.name as EntityBaseName,
							''?'' + ''.'' + s.name + ''.'' + o.name as EntityName,
							case 
								when type=''U''  then 1 
								when type=''V''  then 2 
								when type=''P''  then 3 
								when type=''FN'' then 4 
								when type=''IF'' then 5 
							end as TypeSortOrder,
							case 
								when type=''U''  then ''Table'' 
								when type=''V''  then ''View'' 
								when type=''P''  then ''Sproc'' 
								when type=''FN'' then ''Func'' 
								when type=''IF'' then ''Ifunc'' 
							end as EntityType

						from [?].sys.objects o 
						inner join sys.schemas s on o.schema_id=s.schema_id
						where ''?'' not in (''tempdb'',''master'',''msdb'')
						and type in (''U'',''V'',''P'',''FN'',''IF'')
						and is_ms_shipped = 0'

-- Create a unique id for all Entities (EntityId can be duplicated between databases)
update x
set x.id = x.rn
from (
	select ROW_NUMBER() over (order by EntityName,EntityId) as rn, * from  @AllEntities
) x
		
/**************************************************** ALL DEPENDENCIES *************************************************/
					
-- Get All Dependenies from all DBs

DECLARE @AllDepsRaw table ( UsesId int,
						 UsedById int,	 
						 EntityId int, 
                         DBName nvarchar(30),
						 UsedByEntityName nvarchar(200) );

INSERT INTO @AllDepsRaw (UsesId,UsedById,EntityId,DBName,UsedByEntityName)
	EXEC sp_msforeachdb 
		'
		BEGIN TRY
			select
				0 as UsedId, 0 as UsedById,
				referencing_id as EntityId,
				''?'' as DBName,
					isnull(referenced_database_name,''?'') +  ''.'' + isnull(referenced_schema_name,''dbo'') + ''.'' + referenced_entity_name  as UsedByEntityName
			from [?].sys.sql_expression_dependencies 
			where ''?'' not in (''tempdb'',''master'',''msdb'',''mssqlsystemresource'')
		END TRY
		BEGIN CATCH
		END CATCH
		'

DECLARE @AllDeps table (
	SeqID int,
	ParentId int,
	ParentType nvarchar(10),
	ParentName nvarchar(200),
	ParentTypeOrder int,

	ChildId int,
	ChildType nvarchar(10),
	ChildName nvarchar(200),
	ChildTypeOrder int
	);

-- join to get parent and child names etc
INSERT INTO @AllDeps (SeqId,ParentId,ParentType,ParentName,ParentTypeOrder,ChildId,ChildType,ChildName,ChildTypeOrder)

select  0 as SeqId,
        ao1.id as ParentId,
	    ao1.EntityType as ParentType,
		ao1.EntityName as ParentName,
		ao1.TypeSortOrder as ParentTypeOrder,

		ao2.id as ChildId,
		ao2.EntityType as ChildType,
	    d.UsedByEntityName  as ChildName,
		ao2.TypeSortOrder as ChildTypeOrder
 from  @AllDepsRaw d
 join @AllEntities ao1 on ao1.EntityId = d.EntityId and ao1.dbName = d.DBName
 join @AllEntities ao2 on ao2.EntityName =  d.UsedByEntityName 
 Where ao1.id <> ao2.id -- eliminate contraints
  and  d.UsedByEntityName not Like '%sysdiagrams%'
 Order by ao1.id

 -- Create a unique id for all Entities (Objects can be duplicated between databases)
update x
set x.SeqId = x.rn
from (
	select ROW_NUMBER() over (order by ParentId,ChildName) as rn, * from  @AllDeps
) x
	
-- Delete duplicate objects.

delete from @AllDeps where SeqID in(
select Max(SeqID)
from @AllDeps
group by ParentId,ChildName
having count(*) > 1
)


/**************************************************** FULLDUPS *************************************************/
DECLARE @FullDeps         table (
                          ParentId int,
						  LevelNo int, 
                          ChildId int,
                          ChildTypeOrder int,
						  ChildName nvarchar(100),
						  ChildType nvarchar(6),
						  Seq1 int,
						  Seq2 int,
						  Seq3 int,
						  Seq4 int,
						  Seq5 int,
						  Seq6 int,
						  Seq7 int
						  );

-- Insert Level 1 into @FullDeps
INSERT INTO @FullDeps (ParentId,LevelNo,ChildId,ChildTypeOrder,ChildName,ChildType)
select ParentID,1 as LevelNo,ChildID,ChildTypeOrder,ChildName,ChildType
from @AllDeps

-- Add Sequence for level 1

update x set x.Seq1 = x.rn
from (
	select ROW_NUMBER() over (order by ParentID,ChildTypeOrder,ChildName) as rn, * from  @FullDeps
) x


-- Insert Level 2 into @FullDeps

INSERT INTO @FullDeps (ParentId,LevelNo,ChildId,ChildTypeOrder,ChildName,ChildType,Seq1,Seq2)
select f.ParentID,2 as LevelNo,a.ChildId,a.ChildTypeOrder,a.ChildName,a.ChildType,Seq1,0 as Seq2
from @FullDeps f
join @AllDeps  a on a.ParentId=f.ChildId
where LevelNo=1
order by f.ParentID,Seq1,a.ChildTypeOrder,a.ChildName

-- Add Sequence for level 2
update x set x.Seq2 = x.rn
from (
	select ROW_NUMBER() over (order by ParentID,Seq1,LevelNo,ChildTypeOrder,ChildName) as rn, * from  @FullDeps
) x


-- Insert Level 3 into @FullDeps

INSERT INTO @FullDeps (ParentId,LevelNo,ChildId,ChildTypeOrder,ChildName,ChildType,Seq1,Seq2,Seq3)
select f.ParentID,3 as LevelNo,a.ChildId,a.ChildTypeOrder,a.ChildName,a.ChildType,Seq1,Seq2 ,0 as Seq3 --INC
from @FullDeps f
join @AllDeps  a on a.ParentId=f.ChildId
where LevelNo=2   -- INC
order by f.ParentID,Seq1,a.ChildTypeOrder,a.ChildName

-- Add Sequence for level 3
update x set x.Seq3 = x.rn  --INC
from (
	select ROW_NUMBER() over (order by ParentID,Seq2,LevelNo,ChildTypeOrder,ChildName) as rn, * from  @FullDeps
) x


-- Insert Level 4 into @FullDeps

INSERT INTO @FullDeps (ParentId,LevelNo,ChildId,ChildTypeOrder,ChildName,ChildType,Seq1,Seq2,Seq3,Seq4)
select f.ParentID,4 as LevelNo,a.ChildId,a.ChildTypeOrder,a.ChildName,a.ChildType,Seq1,Seq2,Seq3,0 as Seq4 --INC
from @FullDeps f
join @AllDeps  a on a.ParentId=f.ChildId
where LevelNo=3   -- INC
order by f.ParentID,Seq1,a.ChildTypeOrder,a.ChildName

-- Add Sequence for level 4
update x set x.Seq4 = x.rn  --INC
from (
	select ROW_NUMBER() over (order by ParentID,Seq3,LevelNo,ChildTypeOrder,ChildName) as rn, * from  @FullDeps  --INC
) x


-- Insert Level 5 into @FullDeps

INSERT INTO @FullDeps (ParentId,LevelNo,ChildId,ChildTypeOrder,ChildName,ChildType,Seq1,Seq2,Seq3,Seq4,Seq5)
select f.ParentID,5 as LevelNo,a.ChildId,a.ChildTypeOrder,a.ChildName,a.ChildType,Seq1,Seq2,Seq3,Seq4,0 as Seq5 --INC
from @FullDeps f
join @AllDeps  a on a.ParentId=f.ChildId
where LevelNo=4   -- INC
order by f.ParentID,Seq1,a.ChildTypeOrder,a.ChildName

-- Add Sequence for level 5
update x set x.Seq5 = x.rn  --INC
from (
	select ROW_NUMBER() over (order by ParentID,Seq4,LevelNo,ChildTypeOrder,ChildName) as rn, * from  @FullDeps  --INC
) x


-- Insert Level 6 into @FullDeps

INSERT INTO @FullDeps (ParentId,LevelNo,ChildId,ChildTypeOrder,ChildName,ChildType,Seq1,Seq2,Seq3,Seq4,Seq5,Seq6)
select f.ParentID,6 as LevelNo,a.ChildId,a.ChildTypeOrder,a.ChildName,a.ChildType,Seq1,Seq2,Seq3,Seq4,Seq5,0 as Seq6 --INC
from @FullDeps f
join @AllDeps  a on a.ParentId=f.ChildId
where LevelNo=5   -- INC
order by f.ParentID,Seq1,a.ChildTypeOrder,a.ChildName

-- Add Sequence for level 6
update x set x.Seq6 = x.rn  --INC
from (
	select ROW_NUMBER() over (order by ParentID,Seq5,LevelNo,ChildTypeOrder,ChildName) as rn, * from  @FullDeps  --INC
) x


-- Insert Level 7 into @FullDeps

INSERT INTO @FullDeps (ParentId,LevelNo,ChildId,ChildTypeOrder,ChildName,ChildType,Seq1,Seq2,Seq3,Seq4,Seq5,Seq6,Seq7)
select f.ParentID,7 as LevelNo,a.ChildId,a.ChildTypeOrder,a.ChildName,a.ChildType,Seq1,Seq2,Seq3,Seq4,Seq5,Seq6,0 as Seq7 --INC
from @FullDeps f
join @AllDeps  a on a.ParentId=f.ChildId
where LevelNo=6   -- INC
order by f.ParentID,Seq1,a.ChildTypeOrder,a.ChildName

-- Add Sequence for level 7
update x set x.Seq7 = x.rn  --INC
from (
	select ROW_NUMBER() over (order by ParentID,Seq6,LevelNo,ChildTypeOrder,ChildName) as rn, * from  @FullDeps  --INC
) x

-- Add counts and levels to Objects
DECLARE @LoveToCount table (
	Id int,ChildCount int, ChildLevels int);
INSERT INTO @LoveToCount(id,ChildCount,ChildLevels)
select ParentID,
	   count(*) as ChildCount ,
	   max(LevelNo) as ChildLevels
from @FullDeps group by ParentID


update @AllEntities
Set ChildCount =  ltc.ChildCount,
    ChildLevels = ltc.ChildLevels
from @LoveToCount ltc
join @AllEntities  ao on  ao.id=ltc.id

/**************************************************** EXPORTS *************************************************/

-- dbEntities  -- JSON

select 
 '{ '                  +
 'id : '               + convert(varchar,id)	         + ', '   +
 'DBName : '''         + DBName						     + ''', ' +
 'SchemaName : '''     + SchemaName					     + ''', ' +
 'EntityBaseName : ''' + EntityBaseName				     + ''', ' +
 'EntityName : '''     + EntityName					     + ''', ' +
 'EntityType : '''     + EntityType				     	 + ''', ' +
 'TypeSortOrder : '    + convert(varchar,TypeSortOrder)  + ','  +
 'ChildCount :   '     + convert(varchar,ChildCount)	 + ', ' +
 'ChildLevels :  '     + convert(varchar,ChildCount)	 + ', ' +
   ' },' as dbEntities
 from @AllEntities order by id;

 
 -- dbDependency -- JSON
 
select  
 '{ '                   +
	'ParentId  : ' + convert(varchar,ParentId)					+ ', '   +
	'LevelNo   : ' +  convert(varchar,LevelNo)                  + ', '   +
	'Sequence  : ' +  convert(varchar,Seq7)                     + ', '   +
	'ChildId : ' + convert(varchar,ChildId)						+ ', ' +
	'ChildType: ''' + convert(varchar,ChildType)				+ ''', ' +
    'ChildName : ''' + ChildName						        + ''', ' +
 ' },' as dbDependency    
 from  @FullDeps d
 order by Seq7

 
-- dbEntityTypes  -- JSON

select distinct 
 '{ '										+
	'EntityType : '''  + EntityType						+ ''', ' +
	'TypeSortOrder : ' + convert(varchar,TypeSortOrder)	+ ', '   +
  ' },'  as  dbEntityType
from @AllEntities

