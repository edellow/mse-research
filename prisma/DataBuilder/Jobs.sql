use msdb

;with

JobIDS as (
select ROW_NUMBER() over (order by job_id) as JobID,job_id
   from msdb.dbo.sysjobs
),
jobs as (
	SELECT
	    JobIDS.JobID as job_id,
		sJOB.name AS job_name,
		sJob.description as job_description,
		start_step_id, 
		enabled
	FROM msdb.dbo.sysjobs AS sJOB
	join JobIDS on JobIDS.job_id = sJob.job_id
)

/**************************************************** EXPORTS *************************************************/

select '{ id : ' + convert(varchar,job_id ) + 
        ', Name : ''' + job_name + 
		''', Description : ''' + replace(replace(replace(job_description,CHAR(13),'<br>'),CHAR(10),''),'''','\''') + 
		''' ,StartStep :' + convert(nvarchar,start_step_id)  +
		' ,Enabled :' + convert(nvarchar,enabled)   +
		'},' as dbJobs
		
from jobs
order by job_name


