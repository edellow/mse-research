 -- dbDatabaseNames  -- JSON

/**************************************************** EXPORTS *************************************************/

select 
 '{ '										+
 	'id : ' + convert(varchar,database_id)	+ ', '   +
	'DBName : ''' + name						+ ''', ' +
 ' },'  as dbDataBase					
from sys.databases
where name not in ('tempdb','master','msdb','mssqlsystemresource','model','archived_data')
