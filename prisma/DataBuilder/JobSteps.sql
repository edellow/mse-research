use msdb
;with

JobIDS as (
select ROW_NUMBER() over (order by job_id) as JobID,job_id
   from msdb.dbo.sysjobs
),
StepActions as (
	select 1 as id, 'Quit with success' as description
	union
	select 2 as id, 'Quit with failure' as description
	union
	select 3 as id, 'Go to Next step' as description
	union
	select 4 as id, 'Go to step' as description
),
Packages as (
	SELECT job_id,step_id,
		reverse(left(RevPackage,isnull(nullif(PATINDEX('%\%',RevPackage)-1,-1),len(RevPackage))))+ '.dtsx' as PackageName
		from (
		SELECT
			job_id,step_id,
			case 
				when substring(command,2,3) = 'SQL'      then reverse(substring(command,10, PATINDEX('%SERVER%',command)-15))
				when substring(command,2,4) = 'FILE'     then reverse(replace(substring(command,1,PATINDEX('%.dtsx%',command)-1),'/FILE "\"',''))
				when substring(command,2,8) = 'ISSERVER' then reverse(replace(substring(command,1,PATINDEX('%.dtsx%',command)-1),'/ISSERVER "\"',''))
			end as RevPackage
		FROM msdb.dbo.sysjobsteps step 
		where subsystem = 'SSIS'
	) Packages
),
JobSteps as  (
	select 
		JobIDS.JobID as job_id,
		Steps.step_id, 
		replace(replace(replace(Steps.step_name,CHAR(13),'<br>'),CHAR(10),''),'''','\''') as step_name,
		database_name,
		subsystem,
		ISNULL(Packages.PackageName,'')  as package_name,
		replace(replace(replace(Steps.command,CHAR(13),'<br>'),CHAR(10),''),'''','\''') as command,
		last_run_date,
		on_success_action,
		sa1.description as on_success_desc,
		sa2.description as on_fail_desc,
		on_success_step_id,
		on_fail_action,
		on_fail_step_id
	from msdb.dbo.sysjobsteps as Steps
	join JobIDS on JobIDS.job_id= Steps.job_id
	join StepActions sa1 on sa1.id = Steps.on_success_action
	join StepActions sa2 on sa2.id = Steps.on_fail_action
	left join Packages on Packages.job_id = Steps.job_id and Packages.step_id = Steps.step_id
)

/**************************************************** EXPORTS *************************************************/

--select * from JobSteps


 select 
 '{ '                   +
 'JobID : '             + convert(varchar,job_id)				+ ', ' +
 'StepID : '            + convert(varchar,step_id)				+ ', ' +
 'StepName : '''        + step_name								+ ''', ' +
 'DatabaseName : '''    + isnull(database_name,'')				+ ''', ' +
 'SubSystem : '''       + subsystem								+ ''', ' +
 'PackageName : '''     + package_name							+ ''', ' +
 'Command : '''         + command								+ ''', ' + 
 'LastRunDate : '''     + convert(varchar,last_run_date)  		+ ''', ' +
 'OnSuccessAction : '   + convert(varchar,on_success_action)	+ ', ' +
 'OnSuccessDesc : '''   + on_success_desc						+ ''', ' + 
 'OnFailDesc : '''      + on_fail_desc							+ ''', ' +
 'OnSuccessStepID : '   + convert(varchar,on_success_step_id)	+ ', ' +
 'OnFailAction : '      + convert(varchar,on_fail_action)  		+ ', ' +
 'OnFailStepID : '      + convert(varchar,on_fail_step_id)		+ ', ' +
 ' },'    as dbJobSteps

 from JobSteps 
--  where job_id = 75
