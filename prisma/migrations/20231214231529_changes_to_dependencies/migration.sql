/*
  Warnings:

  - You are about to drop the column `ChildTypeOrder` on the `dbDependency` table. All the data in the column will be lost.
  - You are about to drop the column `ParentName` on the `dbDependency` table. All the data in the column will be lost.
  - You are about to drop the column `ParentType` on the `dbDependency` table. All the data in the column will be lost.
  - You are about to drop the column `ParentTypeOrder` on the `dbDependency` table. All the data in the column will be lost.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_dbDependency" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "ParentId" INTEGER NOT NULL,
    "LevelNo" INTEGER NOT NULL DEFAULT 0,
    "Sequence" INTEGER NOT NULL DEFAULT 0,
    "ChildId" INTEGER NOT NULL,
    "ChildType" TEXT NOT NULL,
    "ChildName" TEXT NOT NULL
);
INSERT INTO "new_dbDependency" ("ChildId", "ChildName", "ChildType", "ParentId", "id") SELECT "ChildId", "ChildName", "ChildType", "ParentId", "id" FROM "dbDependency";
DROP TABLE "dbDependency";
ALTER TABLE "new_dbDependency" RENAME TO "dbDependency";
CREATE INDEX "dbDependency_ParentId_idx" ON "dbDependency"("ParentId");
CREATE INDEX "dbDependency_ChildId_idx" ON "dbDependency"("ChildId");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
