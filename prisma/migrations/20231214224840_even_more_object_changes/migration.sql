/*
  Warnings:

  - You are about to drop the `dbObject` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `dbObjectType` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropIndex
DROP INDEX "dbObject_ObjectName_idx";

-- DropIndex
DROP INDEX "dbObject_TypeDesc_idx";

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "dbObject";
PRAGMA foreign_keys=on;

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "dbObjectType";
PRAGMA foreign_keys=on;

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_dbDependency" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "ParentId" INTEGER NOT NULL,
    "ParentType" TEXT NOT NULL,
    "ParentName" TEXT NOT NULL,
    "ParentTypeOrder" INTEGER NOT NULL,
    "ChildId" INTEGER NOT NULL,
    "ChildType" TEXT NOT NULL,
    "ChildName" TEXT NOT NULL,
    "ChildTypeOrder" INTEGER NOT NULL
);
INSERT INTO "new_dbDependency" ("ChildId", "ChildName", "ChildType", "ChildTypeOrder", "ParentId", "ParentName", "ParentType", "ParentTypeOrder", "id") SELECT "ChildId", "ChildName", "ChildType", "ChildTypeOrder", "ParentId", "ParentName", "ParentType", "ParentTypeOrder", "id" FROM "dbDependency";
DROP TABLE "dbDependency";
ALTER TABLE "new_dbDependency" RENAME TO "dbDependency";
CREATE INDEX "dbDependency_ParentId_idx" ON "dbDependency"("ParentId");
CREATE INDEX "dbDependency_ChildId_idx" ON "dbDependency"("ChildId");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
