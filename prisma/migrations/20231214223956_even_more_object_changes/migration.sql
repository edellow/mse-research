-- CreateTable
CREATE TABLE "dbEntityType" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "EntityType" TEXT NOT NULL,
    "TypeDesc" TEXT NOT NULL,
    "TypeSortOrder" INTEGER NOT NULL
);
