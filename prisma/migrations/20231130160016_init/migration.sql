-- CreateTable
CREATE TABLE "dbDatabase" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "DBName" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "dbObjectType" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "ObjectType" TEXT NOT NULL,
    "TypeDesc" TEXT NOT NULL,
    "TypeSortOrder" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "dbObject" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "DBName" TEXT NOT NULL,
    "SchemaName" TEXT NOT NULL,
    "ObjectBaseName" TEXT NOT NULL,
    "ObjectName" TEXT NOT NULL,
    "ObjectType" TEXT NOT NULL,
    "TypeDesc" TEXT NOT NULL,
    "TypeSortOrder" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "dbDependency" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "ParentId" INTEGER NOT NULL,
    "ParentType" TEXT NOT NULL,
    "ParentName" TEXT NOT NULL,
    "ParentTypeOrder" INTEGER NOT NULL,
    "ChildId" INTEGER NOT NULL,
    "ChildType" TEXT NOT NULL,
    "ChildName" TEXT NOT NULL,
    "ChildTypeOrder" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "dbJob" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "Name" TEXT NOT NULL,
    "Description" TEXT NOT NULL,
    "StartStep" INTEGER NOT NULL,
    "Enabled" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "dbJobStep" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "JobID" INTEGER NOT NULL,
    "StepID" INTEGER NOT NULL,
    "StepName" TEXT NOT NULL,
    "DatabaseName" TEXT NOT NULL,
    "SubSystem" TEXT NOT NULL,
    "PackageName" TEXT NOT NULL,
    "Command" TEXT NOT NULL,
    "LastRunDate" TEXT NOT NULL,
    "OnSuccessAction" INTEGER NOT NULL,
    "OnSuccessDesc" TEXT NOT NULL,
    "OnFailDesc" TEXT NOT NULL,
    "OnSuccessStepID" INTEGER NOT NULL,
    "OnFailAction" INTEGER NOT NULL,
    "OnFailStepID" INTEGER NOT NULL,
    CONSTRAINT "dbJobStep_JobID_fkey" FOREIGN KEY ("JobID") REFERENCES "dbJob" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateIndex
CREATE INDEX "dbObject_TypeDesc_idx" ON "dbObject"("TypeDesc");

-- CreateIndex
CREATE INDEX "dbObject_ObjectName_idx" ON "dbObject"("ObjectName");

-- CreateIndex
CREATE INDEX "dbDependency_ParentId_idx" ON "dbDependency"("ParentId");

-- CreateIndex
CREATE INDEX "dbDependency_ChildId_idx" ON "dbDependency"("ChildId");

-- CreateIndex
CREATE INDEX "dbJobStep_JobID_idx" ON "dbJobStep"("JobID");
