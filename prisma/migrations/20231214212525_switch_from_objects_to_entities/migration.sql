-- CreateTable
CREATE TABLE "dbEntity" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "DBName" TEXT NOT NULL,
    "SchemaName" TEXT NOT NULL,
    "ObjectBaseName" TEXT NOT NULL,
    "ObjectName" TEXT NOT NULL,
    "ObjectType" TEXT NOT NULL,
    "TypeSortOrder" INTEGER NOT NULL,
    "ChildCount" INTEGER NOT NULL,
    "ChildLevels" INTEGER NOT NULL
);

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_dbDependency" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "ParentId" INTEGER NOT NULL,
    "ParentType" TEXT NOT NULL,
    "ParentName" TEXT NOT NULL,
    "ParentTypeOrder" INTEGER NOT NULL,
    "ChildId" INTEGER NOT NULL,
    "ChildType" TEXT NOT NULL,
    "ChildName" TEXT NOT NULL,
    "ChildTypeOrder" INTEGER NOT NULL,
    CONSTRAINT "dbDependency_ChildId_fkey" FOREIGN KEY ("ChildId") REFERENCES "dbObject" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "dbDependency_ParentId_fkey" FOREIGN KEY ("ParentId") REFERENCES "dbObject" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_dbDependency" ("ChildId", "ChildName", "ChildType", "ChildTypeOrder", "ParentId", "ParentName", "ParentType", "ParentTypeOrder", "id") SELECT "ChildId", "ChildName", "ChildType", "ChildTypeOrder", "ParentId", "ParentName", "ParentType", "ParentTypeOrder", "id" FROM "dbDependency";
DROP TABLE "dbDependency";
ALTER TABLE "new_dbDependency" RENAME TO "dbDependency";
CREATE INDEX "dbDependency_ParentId_idx" ON "dbDependency"("ParentId");
CREATE INDEX "dbDependency_ChildId_idx" ON "dbDependency"("ChildId");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;

-- CreateIndex
CREATE INDEX "dbEntity_ObjectType_idx" ON "dbEntity"("ObjectType");

-- CreateIndex
CREATE INDEX "dbEntity_ObjectName_idx" ON "dbEntity"("ObjectName");
