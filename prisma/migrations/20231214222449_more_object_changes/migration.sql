/*
  Warnings:

  - You are about to drop the column `ObjectBaseName` on the `dbEntity` table. All the data in the column will be lost.
  - You are about to drop the column `ObjectName` on the `dbEntity` table. All the data in the column will be lost.
  - You are about to drop the column `ObjectType` on the `dbEntity` table. All the data in the column will be lost.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_dbEntity" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "DBName" TEXT NOT NULL,
    "SchemaName" TEXT NOT NULL,
    "EntityBaseName" TEXT NOT NULL DEFAULT '',
    "EntityName" TEXT NOT NULL DEFAULT '',
    "EntityType" TEXT NOT NULL DEFAULT '',
    "TypeSortOrder" INTEGER NOT NULL,
    "ChildCount" INTEGER NOT NULL,
    "ChildLevels" INTEGER NOT NULL
);
INSERT INTO "new_dbEntity" ("ChildCount", "ChildLevels", "DBName", "SchemaName", "TypeSortOrder", "id") SELECT "ChildCount", "ChildLevels", "DBName", "SchemaName", "TypeSortOrder", "id" FROM "dbEntity";
DROP TABLE "dbEntity";
ALTER TABLE "new_dbEntity" RENAME TO "dbEntity";
CREATE INDEX "dbEntity_EntityType_idx" ON "dbEntity"("EntityType");
CREATE INDEX "dbEntity_EntityName_idx" ON "dbEntity"("EntityName");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
