# Welcome to Remix!

- [Remix Docs](https://remix.run/docs)

## Development

--Execute the steps below to install

-npm init
-npm install
-create .env file in project root with this one line
DATABASE_URL="file:./dev.db"

-npx prisma migrate dev
-npm run dev
-browser -> http://localhost:3000/

-- go to the 'seed' page
-- Check all the boxes and clead 'seed'
-- Go to the jobs page
-- type in the search boxes and press search

-- Look at the data in the database (uses browser)
npx prisma studio
