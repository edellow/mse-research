import { useNavigation } from "@remix-run/react";

type StatusProps = {
  Status: string;
  Default?: string;
};

export function StatusBar({ Status, Default = "" }: StatusProps) {
  const navigation = useNavigation();
  return (
    <div className="statusbar">
      {navigation.state === "submitting" ? Status : Default}
    </div>
  );
}
