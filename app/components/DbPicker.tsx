import { useLoaderData } from "@remix-run/react";
import { db } from "../actions/dbconnect";

export async function loader({ request }) {
  const dbs = await db.dbDatabase.findMany({
    orderBy: [
      {
        DBName: "asc",
      },
    ],
  });

  return { dbs };
}

export default function DBPicker(dbname) {
  const { dbs } = useLoaderData<typeof loader>();
  return (
    <>
      <select name="dbname" defaultValue={dbname ? dbname : ""}>
        <option value="">All databases</option>
        {dbs.map((db) => {
          return (
            <>
              <option key={db.id}>{db.DBName}</option>
            </>
          );
        })}
      </select>
    </>
  );
}
