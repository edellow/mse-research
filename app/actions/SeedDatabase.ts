import { db } from "../actions/dbconnect";
// Seed files
import { dbEntitySeed } from "@/../prisma/seedfiles/dbEntity";
import { dbDatabase } from "@/../prisma/seedfiles/dbDataBase";
import { dbEntityTypeSeed } from "prisma/seedfiles/dbEntityType";
import { dbDependencySeed } from "@/../prisma/seedfiles/dbDependency";
import { dbJobSeed } from "@/../prisma/seedfiles/dbJob";
import { dbJobStepSeed } from "@/../prisma/seedfiles/dbJobStep";

export const SeedDatabase = async (
  doReference,
  doEntities,
  doDependencies,
  doJobs
) => {
  console.log(`SEEDING DATABASE ${doJobs}`);
  if (doReference) {
    await seedReference();
  }
  if (doEntities) {
    await seedEntity();
  }
  if (doDependencies) {
    await seedDependency();
  }
  if (doJobs) {
    await seedJob();
  }

  return { ok: true };
};

export async function seedReference() {
  await db.dbDatabase.deleteMany();
  for (let database of dbDatabase) {
    await db.dbDatabase.create({ data: database });
  }

  await db.dbEntityType.deleteMany();
  for (let refType of dbEntityTypeSeed) {
    await db.dbEntityType.create({ data: refType });
  }
}

export async function seedEntity() {
  await db.dbEntity.deleteMany();
  for (let object of dbEntitySeed) {
    await db.dbEntity.create({ data: object });
  }
}

export async function seedDependency() {
  await db.dbDependency.deleteMany();
  for (let dependency of dbDependencySeed) {
    await db.dbDependency.create({ data: dependency });
  }
}

export const seedJob = async () => {
  await db.dbJobStep.deleteMany();
  await db.dbJob.deleteMany();
  for (let job of dbJobSeed) {
    await db.dbJob.create({ data: job });
  }
  for (let jobStep of dbJobStepSeed) {
    await db.dbJobStep.create({ data: jobStep });
  }
};
