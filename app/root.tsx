import type { LinksFunction } from "@remix-run/node";
import globalStylesUrl from "~/global.css";

import {
  NavLink,
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
} from "@remix-run/react";

export const links: LinksFunction = () => [
  { rel: "stylesheet", href: globalStylesUrl },
];

export default function App() {
  return (
    <Document>
      <Layout>
        <Outlet />
      </Layout>
    </Document>
  );
}

function Document({ children }) {
  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <Meta />
        <Links></Links>
        <title>MSE Documentation</title>
        <Links />
      </head>
      <body className={" bg-slate-800 text-slate-100 container mx-auto"}>
        <div>{children}</div>
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}

function Layout({ children }) {
  return (
    <>
      <nav>
        <h1>MSE Research</h1>
        <NavLink to="/">Dashboard</NavLink>
        <NavLink to="/jobs">Jobs</NavLink>
        <NavLink to="/entities">Entities</NavLink>
        <NavLink to="/seed">Seed</NavLink>
      </nav>
      {children}
    </>
  );
}
