import { Form } from "@remix-run/react";
import { StatusBar } from "~/components/StatusBar";
import { SeedDatabase } from "~/actions/SeedDatabase";

export const action = async ({ request, params }) => {
  const form = await request.formData();
  await SeedDatabase(
    form.get("seedReference") === "on",
    form.get("seedEntities") === "on",
    form.get("seedDependencies"),
    form.get("seedJobs") === "on"
  );
  return { ok: true };
};

export default function SeedPage() {
  return (
    <div>
      <h1 text-3xl font-bold underline>
        Seed Database
      </h1>
      <StatusBar
        Status="Seeding database tables..."
        Default="Select items to be seeded in the database"
      />
      <br />
      <br />
      <Form method="POST">
        <div className="flex flex-col flex-1 gap-1">
          <label className="flex gap-2">
            <input id="seedReference" name="seedReference" type="checkbox" />
            Reference Tables
          </label>

          <label className="flex gap-2">
            <input id="seedEntities" name="seedEntities" type="checkbox" />
            Database Entities
          </label>

          <label className="flex gap-2">
            <input
              id="seedDependencies"
              name="seedDependencies"
              type="checkbox"
            />
            Database Object Dependencies
          </label>

          <label className="flex gap-2">
            <input id="seedJobs" name="seedJobs" type="checkbox" />
            Jobs and Job Steps
          </label>
        </div>
        <br />
        <button type="submit"> Seed</button>
        <br />
        <br />
      </Form>
    </div>
  );
}
