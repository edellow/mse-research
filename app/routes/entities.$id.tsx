// Entities$id

import { useLoaderData } from "@remix-run/react";
import { db } from "~/actions/dbconnect";

export async function loader({ params, request }) {
  const id = parseInt(params.id);
  const entity = await db.dbEntity.findFirst({
    where: {
      id: { equals: id },
    },
  });
  const dependencies = await db.dbDependency.findMany({
    where: {
      ParentId: { equals: id },
    },
    orderBy: [{ Sequence: "asc" }],
  });

  return { entity, dependencies, id };
}

export default function EntitiesDetailPage() {
  const { entity, dependencies, id } = useLoaderData<typeof loader>();
  return (
    <>
      <table className="maintable">
        <thead>
          <tr>
            <th>Detail</th>
          </tr>
        </thead>
      </table>
      <div className="p-2">
        <div className="font-semibold">
          <span className="font-bold">{entity?.EntityType + " - "}</span>
          <span>{entity?.DBName + "."}</span>
          <span className="owner">{entity?.SchemaName + "."}</span>
          <span>{entity?.EntityBaseName}</span>
        </div>
      </div>
      <span className="inlineitem">
        <h3>Children</h3>
      </span>
      <div className="tabcontent">
        <ul>
          {dependencies[0]?.ChildName ? (
            dependencies.map((dep) => (
              <li
                key={dep?.id}
                id={"levelNo" + dep.LevelNo}
                className="hover:bg-slate-500"
              >
                <span className="inlineitem">
                  <img
                    className={"typeicon-" + dep.ChildType}
                    src={"/images/" + dep.ChildType + ".png"}
                    alt="Uses"
                  />
                </span>
                {dep.ChildName}
              </li>
            ))
          ) : (
            <li className="p-3">No children found</li>
          )}
        </ul>
      </div>
      <EntityLegend />
    </>
  );
}

function EntityLegend() {
  return (
    <span className="text-slate-100 space-x-7 text-start">
      <span className="inlineitem text-white ">
        <img
          className={"typeicon-Table"}
          src={"/images/table.png"}
          alt="Uses"
        />
      </span>
      Table
      <span className="inlineitem text-white ">
        <img className={"typeicon-View"} src={"/images/View.png"} alt="Uses" />
      </span>
      View
      <span className="inlineitem text-white ">
        <img
          className={"typeicon-Sproc"}
          src={"/images/Sproc.png"}
          alt="Uses"
        />
      </span>
      Sproc
      <span className="inlineitem text-white ">
        <img className={"typeicon-Func"} src={"/images/Func.png"} alt="Uses" />
      </span>
      Function
      <span className="inlineitem text-white ">
        <img
          className={"typeicon-Ifunc"}
          src={"/images/Ifunc.png"}
          alt="Uses"
        />
      </span>
      Inline Function
    </span>
  );
}
