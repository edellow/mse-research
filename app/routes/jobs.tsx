import {
  useLoaderData,
  Form,
  useSubmit,
  useNavigation,
  Outlet,
  Link,
} from "@remix-run/react";
import { db } from "~/actions/dbconnect";

export async function loader({ request }) {
  const search = new URLSearchParams(new URL(request.url).search);
  const fullQ = new URL(request.url).search;
  const q = search.get("q");
  const qn = search.get("qn");

  const filter = {
    OR: [
      { Description: q ? { contains: q } : { contains: "" } },
      { Name: q ? { contains: q } : { contains: "" } },
    ],
    NOT: {
      OR: [
        { Description: qn ? { contains: qn } : {} },
        { Name: qn ? { contains: qn } : {} },
      ],
    },
  };

  console.log(filter);
  const jobs = await db.dbJob.findMany({
    where: filter,

    orderBy: [
      {
        Name: "asc",
      },
    ],
  });
  return { jobs, q, qn, fullQ };
}

export default function JobsPage() {
  const { jobs, q, qn, fullQ } = useLoaderData();
  const submit = useSubmit();
  const navigation = useNavigation();
  const searching =
    navigation.location &&
    new URLSearchParams(navigation.location.search).has("q");

  return (
    <>
      <header>
        <h1>Jobs</h1>

        <div id="searchbar">
          <div>
            <Form
              id="search-form"
              onChange={(event) => {
                const isFirstSearch = q === null;
                submit(event.currentTarget, {
                  replace: !isFirstSearch,
                });
              }}
              role="search"
            >
              <input
                id="q"
                aria-label="Search contacts"
                defaultValue={q || ""}
                placeholder="Search"
                type="search"
                name="q"
              />

              <input
                id="qn"
                type="search"
                placeholder="But not.."
                name="qn"
                defaultValue={qn ? qn : ""}
              />
              <div aria-hidden hidden={!searching} id="search-spinner" />
            </Form>
          </div>
        </div>
      </header>
      <div className="leftcontent">
        <table className="maintable">
          <thead>
            <tr>
              <th className="w-4/12">Job</th>
              <th className="w-6/12">Description</th>
              <th className="w-1/12">Start</th>
              <th className="w-1/12">On</th>
              <th className="w-1/12">Action</th>
            </tr>
          </thead>
          {jobs.map((job) => (
            <tr key={job.id}>
              <td className="w-4/12">{job.Name}</td>
              <td className="w-6/12">{job.Description}</td>
              <td className="w-1/12">{job.StartStep}</td>
              <td className="w-1/12">{job.Enabled}</td>
              <td className="w-1/12 px-2">
                <span className="inlineitem">
                  <Link to={"/jobs/" + job.id + fullQ} className="leftheader  ">
                    <img
                      src="/images/rectangle-list.png"
                      className="detailicon"
                      alt="Uses"
                    ></img>
                  </Link>
                </span>
              </td>
            </tr>
          ))}
        </table>
      </div>
      <span
        className={
          navigation.state === "loading" && !searching ? "loading" : ""
        }
        id="detail"
      >
        <div className="rightcontent">
          <Outlet />
        </div>
      </span>
      Count : {jobs.length}
    </>
  );
}
