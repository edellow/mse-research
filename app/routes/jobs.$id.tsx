// Jobs$id

import { useLoaderData } from "@remix-run/react";
import { db } from "~/actions/dbconnect";

export async function loader({ params, request }) {
  const id = parseInt(params.id);
  const job = await db.dbJob.findFirst({
    where: {
      id: { equals: id },
    },
  });
  const steps = await db.dbJobStep.findMany({
    where: {
      JobID: { equals: id },
    },
    orderBy: [{ StepID: "asc" }],
  });

  return { job, steps };
}

export default function JobDetailPage() {
  const { job, steps } = useLoaderData<typeof loader>();
  return (
    <>
      <table className="maintable">
        <thead>
          <tr>
            <th>Detail</th>
          </tr>
        </thead>
      </table>
      <div className="p-2">
        <div className="font-semibold">
          <span className="font-bold">{"Job - "}</span>
          <span>{job?.Name + "."}</span>
        </div>
      </div>
      <span className="inlineitem">
        <h3>Steps</h3>
      </span>

      <div className="tabcontent">
        <div className="childtable">
          {steps[0]?.StepName ? (
            steps.map((step) => (
              <div key={step.id} className="jobstep">
                <span className="stepname">
                  Step {step.StepID} - {step.StepName}
                </span>
                <br />
                <span className="label">SubSys:</span>
                <span className="info">{step.SubSystem}</span>
                <span className="label">Database:</span>
                <span className="info">{step.DatabaseName}</span>
                <br />
                {step.PackageName.length ? (
                  <span>
                    <span className="label">Package:</span>
                    <span className="info">{step.PackageName}</span> <br />
                  </span>
                ) : (
                  ""
                )}
                <span className="label">Command</span> <br />
                <pre className="sourcecode">
                  {step.Command.replace(/<br>/g, "\n\r")}
                </pre>
                <span className="label">Success:</span>
                <span className="info">{step.OnSuccessDesc}</span>
                <span className="label">Failure:</span>
                <span className="info">{step.OnFailDesc}</span>
              </div>
            ))
          ) : (
            <div className="p-3">No Job Steps found</div>
          )}
        </div>
      </div>
    </>
  );
}
