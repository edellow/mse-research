// Entities

import {
  useLoaderData,
  Form,
  useSubmit,
  Link,
  Outlet,
  useNavigation,
} from "@remix-run/react";
import { db } from "~/actions/dbconnect";

export async function loader({ params, request }) {
  const fullQ = new URL(request.url).search;
  const search = new URLSearchParams(fullQ);
  const q = search.get("q");
  const qn = search.get("qn");
  const dbname = search.get("dbname");
  const entType = search.get("entType");
  const id = parseInt(params.id);

  const filter = {
    EntityName: q ? { contains: q } : {},
    NOT: { EntityName: qn ? { contains: qn } : {} },
    DBName: dbname ? { contains: dbname } : {},
    EntityType: entType ? { contains: entType } : {},
  };

  const entities = await db.dbEntity.findMany({
    where: filter,
    orderBy: [
      {
        EntityName: "asc",
      },
    ],
  });

  const dbs = await db.dbDatabase.findMany({
    orderBy: [
      {
        DBName: "asc",
      },
    ],
  });
  const types = await db.dbEntityType.findMany({
    orderBy: [
      {
        EntityType: "asc",
      },
    ],
  });

  return { entities, dbs, types, q, qn, dbname, entType, fullQ, id };
}

export default function EntitiesPage() {
  const { entities, dbs, types, q, qn, dbname, entType, fullQ, id } =
    useLoaderData<typeof loader>();
  const navigation = useNavigation();
  const submit = useSubmit();
  const searching =
    navigation.location &&
    new URLSearchParams(navigation.location.search).has("q");

  return (
    <>
      <header>
        <h1>Entities</h1>

        <div id="searchbar">
          <div>
            <Form
              id="search-form"
              onChange={(event) => {
                const isFirstSearch = q === null;
                submit(event.currentTarget, {
                  replace: !isFirstSearch,
                });
              }}
              role="search"
            >
              <input
                id="q"
                aria-label="Search contacts"
                defaultValue={q || ""}
                placeholder="Search"
                type="search"
                name="q"
              />

              <input
                id="qn"
                type="search"
                placeholder="But not.."
                name="qn"
                defaultValue={qn ? qn : ""}
              />
              <select name="dbname" defaultValue={dbname ? dbname : ""}>
                <option value="">All databases</option>
                {dbs.map((db) => {
                  return (
                    <>
                      <option key={db.id}>{db.DBName}</option>
                    </>
                  );
                })}
              </select>
              <select name="entType" defaultValue={entType ? entType : ""}>
                <option value="">All Types</option>
                {types.map((type) => {
                  return (
                    <>
                      <option key={type.id} value={type.EntityType}>
                        {type.TypeDesc}
                      </option>
                    </>
                  );
                })}
              </select>
              <div aria-hidden hidden={!searching} id="search-spinner" />
            </Form>
          </div>
        </div>
      </header>
      <div className="leftcontent">
        <table className="maintable">
          <thead>
            <tr>
              <th className="w-1/12">Type</th>
              <th className="w-3/12">Database</th>
              <th className="w-1/12">Owner</th>
              <th className="w-6/12">Name</th>
              <th className="w-1/12">Action</th>
            </tr>
          </thead>
          {entities.map((entity) => (
            <tr
              key={entity.id}
              className={entity?.id === id ? "bg-slate-500" : "bg-slate-700"}
            >
              <td className="w-1/12">
                {entity.EntityType === "InlineFunction"
                  ? "Ifunc"
                  : entity.EntityType}
              </td>
              <td className="w-3/12">{Truncate(entity.DBName, 21)}</td>
              <td className="w-1/12">{entity.SchemaName}</td>
              <td className="w-6/12">{Truncate(entity.EntityBaseName, 45)}</td>
              <td className="w-1/12 px-2">
                <span className="inlineitem">
                  <Link
                    to={"/entities/" + entity.id + fullQ}
                    className="leftheader"
                  >
                    <img
                      src="/images/rectangle-list.png"
                      className="detailicon"
                      alt="Uses"
                    ></img>
                  </Link>
                </span>
                <span className="font-extralight text-red-300">
                  {entity.ChildCount}
                </span>
              </td>
            </tr>
          ))}
        </table>
      </div>
      <span
        className={navigation.state === "loading" ? "loading" : ""}
        id="detail"
      >
        <div className="rightcontent">
          <Outlet />
        </div>
      </span>
      Count : {entities.length}
    </>
  );
}

function Truncate(str: string, len: number) {
  return str.length < len ? str : str.substring(0, len - 1) + "?";
}
