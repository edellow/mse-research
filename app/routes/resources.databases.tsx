import { useFetcher } from "@remix-run/react";
import { useEffect, useState } from "react";

import { db } from "~/actions/dbconnect";
export async function loader() {
  console.log(
    "*************************** dbPicker **************************"
  );
  const dbs = await db.dbDatabase.findMany({
    orderBy: [
      {
        DBName: "asc",
      },
    ],
  });

  return dbs;
}

export function DBPicker(dbname) {
  const databaseList = useFetcher<typeof loader>();
  const [showDetails, setShowDetails] = useState(false);

  useEffect(() => {
    if (showDetails && databaseList.state === "idle" && !databaseList.data) {
      databaseList.load(`/resources/databases`);
    }
  }, [showDetails, databaseList]);

  return (
    <>
      <select
        name="dbname"
        defaultValue={dbname ? dbname : ""}
        onMouseEnter={() => setShowDetails(true)}
        onMouseLeave={() => setShowDetails(false)}
      >
        <option value="">All databases</option>
        {databaseList.data?.map((db) => {
          return (
            <>
              <option key={db.id}>{db.DBName}</option>
            </>
          );
        })}
      </select>
    </>
  );
}
