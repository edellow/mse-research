export default function JobsDefaultDetailPage() {
  return (
    <>
      <table className="maintable">
        <thead>
          <tr>
            <th>Detail</th>
          </tr>
        </thead>
      </table>
      <br></br>
      <span className="inlineitem p-3">Click on an </span>
      <img
        src="/images/rectangle-list.png"
        className="detailicon inlineitem"
        alt="Uses"
      ></img>
      icon in the left panel to show detail.
    </>
  );
}
